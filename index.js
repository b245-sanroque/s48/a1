console.log("HI!");

let posts = [];
// post will serve as our mock database.
	//array of objects
	/*
		{
			id: value,
			title: value,
			body: value
		}
	*/

let count = 1;
	document.querySelector("#form-add-post").addEventListener("submit", (event) => {
		//preventDefault() function stops the auto reload of the webpage when submitting
		event.preventDefault();

		let newPost = {
				id: count,
				title: document.querySelector("#txt-title").value,
				body: document.querySelector("#txt-body").value
		}
		console.log(newPost);

		posts.push(newPost);
		console.log(posts);

		//to have auto increment
		count++
		showPosts(posts);

		document.querySelector("#txt-title").value = "";
		document.querySelector("#txt-body").value = "";

	})

//Show posts
	const showPosts = (posts) => {
		let postEntries = ``;

		posts.forEach((post) => {
			postEntries += 
			`
				<div id = "post-${post.id}">
					<h3 id = "post-title-${post.id}"> ${post.title} </h3>
					<p id = "post-body-${post.id}"> ${post.body} </p>
					<button onclick = "editPost(${post.id})"> Edit </button>
					<button onclick = "deletePost(${post.id})"> Delete </button>
				</div>
			`
		})
		document.querySelector("#div-post-entries").innerHTML = postEntries;
	}

// Edit Post
	const editPost = (id) => {
		let title = document.querySelector(`#post-title-${id}`).innerHTML
		let body = document.querySelector(`#post-body-${id}`).innerHTML
		console.log(title)
		console.log(body)

		document.querySelector("#txt-edit-id").value = id;
		document.querySelector("#txt-edit-title").value = title;
		document.querySelector("#txt-edit-body").value = body;

	}

	document.querySelector("#form-edit-post").addEventListener("submit", (event) => {
		event.preventDefault();

		//forEach to check/find the docto be edited
		posts.forEach(post => {
			let idToBeEdited = document.querySelector("#txt-edit-id").value;
			console.log(typeof idToBeEdited);
			console.log(typeof post.id);
			if(post.id == idToBeEdited){
				let title = document.querySelector("#txt-edit-title").value;
				let body = document.querySelector("#txt-edit-body").value;

				posts[post.id-1].title = title;
				posts[post.id-1].body = body;

				alert("Edit is successful!")
				showPosts(posts);

				document.querySelector("#txt-edit-id").value = "";
				document.querySelector("#txt-edit-title").value = "";
				document.querySelector("#txt-edit-body").value =  "";

			}
		})
	})

// Delete Post
	const deletePost = (id) => {
		// console.log("dltbtn");
		// console.log(`${id}`);
		// posts = document.querySelector(`#div-post-entries`);
		// console.log(posts);

		// console.log(posts);
		// let are = `${id}`
		// console.log(are)
		// console.log(typeof are)

		const index = posts.findIndex((post) => post.id === id);
		console.log(index)
		console.log(typeof index)

		if(index !== -1) {
		posts.splice(index, 1)
		}
		console.log(posts)

	showPosts(posts)
	}

